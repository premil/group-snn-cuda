#include <cstdio>
#include <cstdlib>
#include <ctime>

#include <vector>
#include <set>

#include <cuda_runtime.h>

#define VERBOSE  true

#define MAX_ITERATIONS          10000
#define THREADS_PER_BLOCK         256

// number of groups including the input and output groups
#define GROUP_COUNT                25

#define NEURONS_IN_GROUP         1000
#define GROUP_SYNAPSE_CAPACITY  35000

// share of synapses from outside group (number between 0.0 and 1.0)
#define INPUT_SYNAPSES_RATIO        0.05

#define NEURON_COUNT  (GROUP_COUNT * NEURONS_IN_GROUP)

#define MAX_WEIGHT_F                1.0
#define MAX_SIGNAL_F              100.0

// average input should be roughly 0.25 * avg input count * max signal
// this reguires above average input to reach the treshold (but the probability is foolish to even guess)
#define MAX_TRESHOLD_F  (0.75 * GROUP_SYNAPSE_CAPACITY / (float)NEURONS_IN_GROUP * MAX_SIGNAL_F * MAX_WEIGHT_F)

// probability that an input neuron will fire in an iteration
#define INPUT_SIGNAL_PROB_F         0.1

// leave out the output group
#define MAX_HIDDEN_NEURON_ID  (NEURON_COUNT - NEURONS_IN_GROUP - 1)

#define INPUT_GROUP_ID   0
#define OUTPUT_GROUP_ID  (GROUP_COUNT - 1)

// number of computation slots when all groups are active
#define GROUP_SLOTS  (((NEURONS_IN_GROUP + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK) * GROUP_COUNT)

#define MIN(a, b)  ((a) < (b) ? (a) : (b))

// synapses leading to other groups (one group link for all synapses from one neuron to that group)
// bounded by number of groups and by number of synapses, select he smaller bound
#define GROUP_OUT_LINKS_CAPACITY  MIN(\
	(int)(NEURONS_IN_GROUP * (GROUP_COUNT + 1) * (INPUT_SYNAPSES_RATIO + 0.65)), \
	GROUP_SYNAPSE_CAPACITY)

typedef enum
{
	OK = 0,
	ERROR
}
err_t;

typedef int neuronId_t;
typedef int groupId_t;

typedef std::vector< std::set<groupId_t> > GroupSetVector;

// end of list
#define LIST_END  -1


// ## testing tools and helper functions ##
#define ASSERT_EQ(expected, actual, msg)  if ((actual) != (expected)) \
		{ printf("FAIL: %s != %s (%s)\n", #actual, #expected, msg); return ERROR; }

#define ASSERT(condition, msg)  if (!(condition)) \
		{ printf("FAIL: '%s' does not hold (%s)\n", #condition, msg); return ERROR; }

err_t testParamAssertions()
{
	ASSERT(GROUP_COUNT <= THREADS_PER_BLOCK, "so large group count is not supported by the CUDA kernel");

	ASSERT((INPUT_SYNAPSES_RATIO > 0.0) && (INPUT_SYNAPSES_RATIO < 1.0), "INPUT_SYNAPSES_RATIO out of range");

	return OK;
}

float getRandomFloat(const float max)
{
	return (rand() / (float)RAND_MAX) * max;
}

int getRandomInt(const int max)
{
	if (max < 0) return 0;

	return rand() % (max + 1);
}

err_t testRandomFunctions()
{
	ASSERT((float)RAND_MAX != 0.0, "(float)RAND_MAX would cause div by zero");

	for (int i = 0; i < 100; i++)
	{
		float randomFloat = getRandomFloat((float)i);
		int   randomInt   = getRandomInt(i);

		ASSERT(randomFloat >= 0.0 && randomFloat <= (float)i, "random float out of range");
		ASSERT(randomInt >= 0 && randomInt <= i, "random int out of range");
	}

	return OK;
}

void handleCudaError(const cudaError_t cerr, const char* const file, const int line)
{
	printf("%s(%d): CUDA error #%d. Aborting.\n", file, line, static_cast<unsigned int>(cerr));
	cudaDeviceReset();  // (may produce an annoying screen flicker)
}

#define RETURN_IF_CUDA_ERR(cerr) \
		if ((cerr) != cudaSuccess) { handleCudaError(cerr, __FILE__, __LINE__); return ERROR; }


// ## main data structures ##

// arrays with neuron states
// for simplicity all neurons are stored in a single object as activeSignals[] is accessed from all groups
template <int NEURON_CAPACITY, int INPUT_NEURON_COUNT_ARG>
struct NeuronsT
{
	float tresholds[NEURON_CAPACITY];      //< neuron tresholds (const after initialization)
	float baseSignals[NEURON_CAPACITY];    //< values to be copied to activeSignals for active neurons (const after init)
	float activeSignals[NEURON_CAPACITY];  //< signal or zero for inactive neurons (to save one multiplication for each synapse)
	float acs[NEURON_CAPACITY];            //< action potentials

	__host__
	void init()
	{
		for (int i = 0; i < NEURON_CAPACITY; i++)
		{
			tresholds[i]   = getRandomFloat(MAX_TRESHOLD_F);
			baseSignals[i] = getRandomFloat(MAX_SIGNAL_F);
			activeSignals[i] = 0.0;
			acs[i]           = 0.0;
		}
	}

	// emulates sending random signal from input neurons with probability INPUT_SIGNAL_PROB_F
	__host__
	void generateInputSignals()
	{
		for (neuronId_t n = 0; n < INPUT_NEURON_COUNT_ARG; n++)
		{
			if (getRandomFloat(1.0) < INPUT_SIGNAL_PROB_F)
			{
				baseSignals[n] = getRandomFloat(MAX_SIGNAL_F);
				acs[n]         = tresholds[n] + 1.0f;  // mark for activation
			}
			else
			{
				acs[n] = 0.0f;  // ac is reset in cuda, but we do not copy it back to host memory
			}
		}
	}
};

typedef NeuronsT<NEURON_COUNT, NEURONS_IN_GROUP> AllNeurons;

// stores incoming synapses for one group of neurons and lists of groups that are connected to these neurons
template <int NEURON_CAPACITY, int SYNAPSE_CAPACITY, int OUT_GROUPS_CAPACITY>
class NeuronGroupT
{
public:
	NeuronGroupT(): count(NEURON_CAPACITY), fromIdx(0)
	{}

	// lists of incoming synapses, each starts at index stored at synapseStartIndexes and is terminated by LIST_END
	neuronId_t inputSynapseLists[SYNAPSE_CAPACITY];

	// synapse weights at indexes corresponding to inputSynapseLists
	float weights[SYNAPSE_CAPACITY];

	// lists of outgoing groups, they start at indexes stored at groupStartIndexes and are terminated by LIST_END
	groupId_t outputGroupLists[OUT_GROUPS_CAPACITY];

	__device__
	int getSynapseListStartIdx(neuronId_t neuronId) const
	{
		return synapseStartIndexes[neuronId - fromIdx];
	}

	__device__
	int getGroupListStartIdx(neuronId_t neuronId) const
	{
		return groupStartIndexes[neuronId - fromIdx];
	}

	__host__
	groupId_t getId() const
	{
		return myId;
	}

	__host__
	int getNeuronCount() const
	{
		return count;
	}

	__host__
	neuronId_t getFromIdx() const
	{
		return fromIdx;
	}

	__host__
	err_t init(const groupId_t groupId, const neuronId_t fromIdx, GroupSetVector& outputGroupsOfNeurons);

	__host__
	err_t fillOutputGroupLinks(GroupSetVector& outputGroupsOfNeurons);

private:
	// first indexes of synapse lists of each neuron, see inputSynapseLists
	// this array is indexed with offset: neuronId - fromIdx
	int synapseStartIndexes[NEURON_CAPACITY];

	// first indexes of group lists of each neuron, see outputGroupLists
	// this array is indexed with offset: neuronId - fromIdx
	int groupStartIndexes[NEURON_CAPACITY];

	groupId_t myId;
	neuronId_t fromIdx;
	const int count;

	__host__
	err_t addNeuronSynapses(
			const int localNeuronIdx, const std::vector<neuronId_t>& synapseList, int& newSynapseIdx_inout);

	friend err_t testAddNeuronSynapses();
	friend err_t testInitInputGroup();
};

template <int NEURON_CAPACITY, int SYNAPSE_CAPACITY, int OUT_GROUPS_CAPACITY>
err_t NeuronGroupT<NEURON_CAPACITY, SYNAPSE_CAPACITY, OUT_GROUPS_CAPACITY>::addNeuronSynapses(
		const int localNeuronIdx, const std::vector<neuronId_t>& synapseList, int& newSynapseIdx_inout)
{
	if ((localNeuronIdx < 0) || (localNeuronIdx >= count) || (newSynapseIdx_inout < 0))
	{
		printf("(%d): argument out of range.\n", __LINE__);
		return ERROR;
	}
	else if (newSynapseIdx_inout + synapseList.size() >= SYNAPSE_CAPACITY)
	{
		// this should not happen, but do not crash if it does
		printf("(%d): warning: synapse capacity reached, neuron %d will not be connected.\n", __LINE__,
				fromIdx + localNeuronIdx);

		// check that inputSynapseLists[newSynapseIdx_inout - 1] contains LIST_END
		if (newSynapseIdx_inout < 1 || inputSynapseLists[newSynapseIdx_inout - 1] != LIST_END) return ERROR;

		synapseStartIndexes[localNeuronIdx] = newSynapseIdx_inout - 1;  // mark this neuron as "not connected"
		return OK;
	}

	synapseStartIndexes[localNeuronIdx] = newSynapseIdx_inout;

	for (std::vector<neuronId_t>::const_iterator it = synapseList.begin(); it != synapseList.end(); ++it)
	{
		inputSynapseLists[newSynapseIdx_inout] = *it;
		weights[newSynapseIdx_inout] = getRandomFloat(MAX_WEIGHT_F);
		newSynapseIdx_inout++;
	}

	inputSynapseLists[newSynapseIdx_inout++] = LIST_END;

	return OK;
}

template <int NEURON_CAPACITY, int SYNAPSE_CAPACITY, int OUT_GROUPS_CAPACITY>
err_t NeuronGroupT<NEURON_CAPACITY, SYNAPSE_CAPACITY, OUT_GROUPS_CAPACITY>::init(
		const groupId_t groupId, const neuronId_t fromIdx, GroupSetVector& outputGroupsOfNeurons)
{
	this->myId = groupId;
	this->fromIdx = fromIdx;

	// suppose that average will be 1/2 of maximum and aim for 15 % of free space in the synapse buffer
	const int maxSynapsesPerNeuron = (int)(2.0 * 0.85 * SYNAPSE_CAPACITY / NEURON_CAPACITY - 1.0);

	const int maxSynapsesInsideGroup  = (int)((1.0 - INPUT_SYNAPSES_RATIO) * maxSynapsesPerNeuron);
	const int maxSynapsesOutsideGroup = (int)(INPUT_SYNAPSES_RATIO         * maxSynapsesPerNeuron);

	for (int n = 0, newSynapseIdx = 0; n < count; n++)
	{
		// NOTE: to avoid duplicate synapses simply replace the vector by a set
		std::vector<neuronId_t> synapses;

		// (no incoming synapses for neurons in the input group)
		const int synapsesInsideGroup  = (groupId == INPUT_GROUP_ID) ? 0 : getRandomInt(maxSynapsesInsideGroup);
		const int synapsesOutsideGroup = (groupId == INPUT_GROUP_ID) ? 0 : getRandomInt(maxSynapsesOutsideGroup);

		synapses.reserve(synapsesInsideGroup + synapsesOutsideGroup);

		for (int s = 0; s < synapsesInsideGroup + synapsesOutsideGroup; s++)
		{
			const neuronId_t sourceNeuronId = (s < synapsesInsideGroup) ?
					(fromIdx + getRandomInt(count - 1))  // synapse within group
					:
					getRandomInt(MAX_HIDDEN_NEURON_ID);  // synapse from outside (or possibly within)

			synapses.push_back(sourceNeuronId);
			outputGroupsOfNeurons[sourceNeuronId].insert(myId);  // will be processed after all groups' init
		}

		err_t err = addNeuronSynapses(n, synapses, newSynapseIdx);
		if (err) return err;

		if (n == count - 1)
		{
			printf("Added %d out of maximum %d synapses (avg %.1f).\n", newSynapseIdx - count, SYNAPSE_CAPACITY - count,
					newSynapseIdx / (float)count - 1.0);  // exclude lists terminations
		}
	}

	return OK;
}

template <int NEURON_CAPACITY, int SYNAPSE_CAPACITY, int OUT_GROUPS_CAPACITY>
err_t NeuronGroupT<NEURON_CAPACITY, SYNAPSE_CAPACITY, OUT_GROUPS_CAPACITY>::fillOutputGroupLinks(
		GroupSetVector& outputGroupsOfNeurons)
{
	for (int n = 0, newGroupLinkIdx = 0; n < count; n++)
	{
		const neuronId_t neuronId = fromIdx + n;

		if (newGroupLinkIdx + outputGroupsOfNeurons[neuronId].size() >= OUT_GROUPS_CAPACITY)
		{
			// this should not happen, but handle it anyway
			printf("(%d): warning: output groups buffer capacity reached, skipping links of neuron %d\n", __LINE__,
					neuronId);

			// check that outputGroupLists[newGroupLinkIdx - 1] contains LIST_END
			if (newGroupLinkIdx < 1 || outputGroupLists[newGroupLinkIdx - 1] != LIST_END) return ERROR;

			groupStartIndexes[n] = newGroupLinkIdx - 1; // this neuron will not activate any groups
			continue;
		}

		groupStartIndexes[n] = newGroupLinkIdx;

		for (std::set<groupId_t>::const_iterator it = outputGroupsOfNeurons[neuronId].begin();
				it != outputGroupsOfNeurons[neuronId].end(); ++it)
		{
			outputGroupLists[newGroupLinkIdx++] = *it;
		}

		outputGroupLists[newGroupLinkIdx++] = LIST_END;

		if (n == count - 1)
		{
			printf("Added %d out of maximum %d group links (avg %.1f).\n", newGroupLinkIdx, OUT_GROUPS_CAPACITY,
					newGroupLinkIdx / (float)count - 1.0);
		}
	}

	return OK;
}

typedef NeuronGroupT<NEURONS_IN_GROUP, GROUP_SYNAPSE_CAPACITY, GROUP_OUT_LINKS_CAPACITY> NeuronGroup;

err_t testAddNeuronSynapses()
{
	NeuronGroupT<2, 7, 1> neuronGroup;
	std::vector<neuronId_t> synapses;
	int newSynapseIdx = 0;

	synapses.push_back(1);
	synapses.push_back(2);
	err_t err = neuronGroup.addNeuronSynapses(0, synapses, newSynapseIdx);
	ASSERT_EQ(OK, err, "calling NeuronGroupT::addNeuronSynapses");
	ASSERT_EQ(2, neuronGroup.inputSynapseLists[1], "wrong neuron id");
	ASSERT_EQ(LIST_END, neuronGroup.inputSynapseLists[2], "end of list expected");

	synapses.push_back(3);
	err = neuronGroup.addNeuronSynapses(1, synapses, newSynapseIdx);
	ASSERT_EQ(OK, err, "calling NeuronGroupT::addNeuronSynapses");
	ASSERT_EQ(LIST_END, neuronGroup.inputSynapseLists[2], "end of list expected");
	ASSERT_EQ(LIST_END, neuronGroup.inputSynapseLists[6], "end of list expected");
	ASSERT_EQ(3, neuronGroup.synapseStartIndexes[1], "wrong start index");

	return OK;
}

err_t testInitInputGroup()
{
	NeuronGroupT<2, 7, 1> neuronGroup;
	GroupSetVector outputGroupsOfNeurons;

	err_t err = neuronGroup.init(INPUT_GROUP_ID, 0, outputGroupsOfNeurons);
	ASSERT_EQ(OK, err, "calling NeuronGroupT::init");
	ASSERT_EQ(0, neuronGroup.synapseStartIndexes[0], "first neuron's list should start at the start");
	ASSERT_EQ(LIST_END, neuronGroup.inputSynapseLists[0], "first neuron's list should be empty");

	// the same for the second neuron
	ASSERT_EQ(1, neuronGroup.synapseStartIndexes[1], "wrong start index");
	ASSERT_EQ(LIST_END, neuronGroup.inputSynapseLists[1], "the neuron's list should be empty");

	return OK;
}

// stores all neurons and all neuron groups (which store synapses)
// neurons and neuronGroups are plain members (rather than just pointers) for easy allocation
class Network
{
public:
	__device__
	AllNeurons * getNeurons()
	{
		return &neurons;
	}

	__host__ __device__
	const NeuronGroup * getNeuronGroup(const groupId_t groupId) const
	{
		return &neuronGroups[groupId];
	}

	__host__
	err_t init();

	__host__
	void generateInputSignals()
	{
		neurons.generateInputSignals();
	}

	// make the copy in member function to keep encapsulation (neurons field is a private member)
	__host__
	err_t copyInputSignalsToCudaDevice(Network * d_network);

	// again make the copy here to preserve encapsulation
	__host__
	err_t copyOutputSignalsFromCudaDevice(Network * d_network);

	__host__
	void printActiveOutputNeurons();

private:
	AllNeurons neurons;

	// the first one is the input group, the last one is the output group
	NeuronGroup neuronGroups[GROUP_COUNT];
};

err_t Network::init()
{
	neurons.init();

	GroupSetVector outputGroupsOfNeurons;
	outputGroupsOfNeurons.resize(NEURON_COUNT);

	// init synapses
	neuronId_t fromIdx = 0;
	for (int g = 0; g < GROUP_COUNT; g++)
	{
		err_t err = neuronGroups[g].init(g, fromIdx, outputGroupsOfNeurons);
		if (err) return err;

		fromIdx += NEURONS_IN_GROUP;
	}

	for (int g = 0; g < GROUP_COUNT; g++)
	{
		err_t err = neuronGroups[g].fillOutputGroupLinks(outputGroupsOfNeurons);
		if (err) return err;
	}

	return OK;
}

err_t Network::copyInputSignalsToCudaDevice(Network * d_network)
{
	cudaError_t cerr;
	const int groupArraySize = NEURONS_IN_GROUP * sizeof(float);  // copy part of the array for one group

	cerr = cudaMemcpy(&d_network->neurons.baseSignals, &neurons.baseSignals, groupArraySize, cudaMemcpyHostToDevice);
	RETURN_IF_CUDA_ERR(cerr);

	cerr = cudaMemcpy(&d_network->neurons.acs, &neurons.acs, groupArraySize, cudaMemcpyHostToDevice);
	RETURN_IF_CUDA_ERR(cerr);

	return OK;
}

err_t Network::copyOutputSignalsFromCudaDevice(Network * d_network)
{
	cudaError_t cerr;
	const int groupArraySize = NEURONS_IN_GROUP * sizeof(float);  // copy part of the array for one group

	cerr = cudaMemcpy(&neurons.activeSignals[MAX_HIDDEN_NEURON_ID + 1],
			&d_network->neurons.activeSignals[MAX_HIDDEN_NEURON_ID + 1], groupArraySize, cudaMemcpyDeviceToHost);
	RETURN_IF_CUDA_ERR(cerr);

	return OK;
}

void Network::printActiveOutputNeurons()
{
	bool first = true;

	for (int n = MAX_HIDDEN_NEURON_ID + 1; n < NEURON_COUNT; n++)
	{
		if (neurons.activeSignals[n] > 0.0)
		{
			printf("%s%d", first ? "Active output neurons: " : ", ", n);
			first = false;
		}
	}

	if (!first)
		printf("\n");
}

struct ActiveGroup
{
	int groupId;
	neuronId_t fromIdx;
	int count;
};

// tracks which neuron groups are active and manages which thread block works on which part of a neuron group
class GroupScheduler
{
public:
	bool activeGroupsMap[GROUP_COUNT];

	GroupScheduler(): scheduledSlotCount(0)
	{
		for (int g = 0; g < GROUP_COUNT; g++)
			activeGroupsMap[g] = false;
	}

	__device__
	groupId_t getGroupId(const int blockIdx) const
	{
		return simulationQueue[blockIdx].groupId;
	}

	__device__
	int getNeuronCountInSlot(const int blockIdx) const
	{
		return simulationQueue[blockIdx].count;
	}

	__device__
	neuronId_t getNeuronId(const int blockIdx, const int threadIdx) const
	{
		return simulationQueue[blockIdx].fromIdx + threadIdx;
	}

	__host__
	int getSlotCount()
	{
		return scheduledSlotCount;
	}

	// schedules neuron group to be processed in the next interation
	// if the group is larger than a thread block it is split into multiple parts
	// (it supports groups of different sizes even though this program does not make use of the feature)
	__host__
	void scheduleGroup(const NeuronGroup * group);

	// resets the simulation queue and fills it according to activeGroupsMap[]
	__host__
	void scheduleNewIterationAndResetMap(const Network * network);

	__host__
	void printScheduledGroups();

private:
	ActiveGroup simulationQueue[GROUP_SLOTS];

	int scheduledSlotCount;  // number of groups slots scheduled for simulation
};

void GroupScheduler::scheduleGroup(const NeuronGroup * group)  // TODO: unit test
{
	const int slotCount = (group->getNeuronCount() + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;

	if (scheduledSlotCount + slotCount > GROUP_SLOTS)  // should never happen
	{
		printf("(%d) error: simulation queue capacity reached. Skipping this group.\n", __LINE__);
		return;
	}

	int remainingNeuronCount = group->getNeuronCount();
	neuronId_t fromIdx       = group->getFromIdx();

	for (int i = 0; i < slotCount; i++)
	{
		simulationQueue[scheduledSlotCount].groupId = group->getId();
		simulationQueue[scheduledSlotCount].fromIdx = fromIdx;
		simulationQueue[scheduledSlotCount].count   = MIN(remainingNeuronCount, THREADS_PER_BLOCK);

		remainingNeuronCount -= THREADS_PER_BLOCK;
		fromIdx              += THREADS_PER_BLOCK;
		scheduledSlotCount++;
	}
}

void GroupScheduler::scheduleNewIterationAndResetMap(const Network * network)
{
	scheduledSlotCount = 0;  // reset simulation queue

	for (groupId_t g = 0; g < GROUP_COUNT; g++)
	{
		if (activeGroupsMap[g])
		{
			scheduleGroup(network->getNeuronGroup(g));

			// reset the map (can be made unnecessary by a simple tweak of the propagateSignals() kernel)
			activeGroupsMap[g] = false;
		}
	}
}

void GroupScheduler::printScheduledGroups()
{
	int count = 0;

	for (groupId_t g = 0; g < GROUP_COUNT; g++)
	{
		if (activeGroupsMap[g])
			printf("%s%d", (count++ == 0) ? "Scheduled groups: " : ", ", g);
	}

	if (count > 0)
		printf(" [count: %d]\n", count);
}


// ## cuda kernels ##

// increases action potential of the neurons by the sum of input signals
__global__
void sumSignals(Network * network, GroupScheduler * groupScheduler)
{
	if (threadIdx.x >= groupScheduler->getNeuronCountInSlot(blockIdx.x))
		return;

	const NeuronGroup * neuronGroup = network->getNeuronGroup(groupScheduler->getGroupId(blockIdx.x));
	const neuronId_t neuronId = groupScheduler->getNeuronId(blockIdx.x, threadIdx.x);
	AllNeurons * neurons = network->getNeurons();

	float ac = neurons->acs[neuronId];

	for (int s = neuronGroup->getSynapseListStartIdx(neuronId); neuronGroup->inputSynapseLists[s] != LIST_END; s++)
	{
		ac += neurons->activeSignals[neuronGroup->inputSynapseLists[s]] * neuronGroup->weights[s];
	}

	neurons->acs[neuronId] = ac;  // save updated value to global memory
}

// resets all neurons back to the inactive state (after processing their signals)
__global__
void resetActiveSignals(Network * network)
{
	const int index = blockIdx.x * blockDim.x + threadIdx.x;

	if (index >= NEURON_COUNT)
		return;

	network->getNeurons()->activeSignals[index] = 0.0f;
}

// returns new state of the processed neuron, 'false' for inactive, 'true' for active
__device__
bool activateNeuron(AllNeurons * neurons, const neuronId_t neuronId)
{
	float ac = neurons->acs[neuronId];

	// prevent branching "if (ac > neurons->tresholds[neuronId])..."
	const int isActive = (int)(ac > neurons->tresholds[neuronId]);  // 1 or 0

	neurons->acs[neuronId] = (1 - isActive) * ac;  // reset ac on activation & save updated value to global memory
	neurons->activeSignals[neuronId] = isActive * neurons->baseSignals[neuronId];  // activate neuron

	return (bool)isActive;
}

__device__
void activateGroups(const NeuronGroup * neuronGroup, bool sharedActiveGroupsMap[], const neuronId_t neuronId,
		const bool isActive)
{
	if (isActive)
	{
		for (int g = neuronGroup->getGroupListStartIdx(neuronId); neuronGroup->outputGroupLists[g] != LIST_END; g++)
		{
			// (potentially write the same value from multiple threads within blocks)
			sharedActiveGroupsMap[neuronGroup->outputGroupLists[g]] = true;
		}
	}
}

// checks AC against the treshold, activates selected neurons
// and marks all groups that are connected to the selected neurons for processing in the next iteration
__global__
void propagateSignals(Network * network, GroupScheduler * groupScheduler)
{
	__shared__ bool sharedActiveGroupsMap[GROUP_COUNT];

	const int tid = threadIdx.x;
	if (tid < GROUP_COUNT)  // (assume we have more threads than neuron groups)
		sharedActiveGroupsMap[tid] = false;  // NOTE: is there a better way to init the array?
	__syncthreads();

	if (tid < groupScheduler->getNeuronCountInSlot(blockIdx.x))
	{
		const NeuronGroup * neuronGroup = network->getNeuronGroup(groupScheduler->getGroupId(blockIdx.x));
		const neuronId_t neuronId = groupScheduler->getNeuronId(blockIdx.x, tid);

		const bool isActive = activateNeuron(network->getNeurons(), neuronId);

		activateGroups(neuronGroup, sharedActiveGroupsMap, neuronId, isActive);
	}

	// save to global memory
	__syncthreads();
	if ((tid < GROUP_COUNT) && sharedActiveGroupsMap[tid])
		groupScheduler->activeGroupsMap[tid] = true;  // (potentially write the same value from multiple blocks)
}

err_t simulationMain()
{
	Network * h_network = new Network();
	GroupScheduler * h_groupScheduler = new GroupScheduler();

	if (h_network == NULL || h_groupScheduler == NULL)
	{
		printf("(%d): Failed to allocate host data.", __LINE__);
		return ERROR;
	}

	err_t err = h_network->init();
	if (err) return err;

	cudaError_t cerr;
	Network * d_network;
	GroupScheduler * d_groupScheduler;

	cerr = cudaMalloc((void **) &d_network, sizeof(Network));
	RETURN_IF_CUDA_ERR(cerr);

	cerr = cudaMalloc((void **) &d_groupScheduler, sizeof(GroupScheduler));
	RETURN_IF_CUDA_ERR(cerr);

	cerr = cudaMemcpy(d_network, h_network, sizeof(Network), cudaMemcpyHostToDevice);
	RETURN_IF_CUDA_ERR(cerr);

	for (int i = 0; i < MAX_ITERATIONS; i++)
	{
		h_network->generateInputSignals();
		err = h_network->copyInputSignalsToCudaDevice(d_network);
		if (err) return err;

		h_groupScheduler->activeGroupsMap[INPUT_GROUP_ID] = true;  // always schedule the input group
		if (VERBOSE)
		{
			printf("Iteration %d\n", i);
			h_groupScheduler->printScheduledGroups();
		}

		const bool wasOutputGroupScheduled = h_groupScheduler->activeGroupsMap[OUTPUT_GROUP_ID];

		h_groupScheduler->scheduleNewIterationAndResetMap(h_network);

		cerr = cudaMemcpy(d_groupScheduler, h_groupScheduler, sizeof(GroupScheduler), cudaMemcpyHostToDevice);
		RETURN_IF_CUDA_ERR(cerr);

		const int scheduledBlocks = h_groupScheduler->getSlotCount();

		// run sumSignals and propagateSignals separately for synchronisation (over activeSignals[])
		// sum input signals and update AC
		sumSignals<<<scheduledBlocks, THREADS_PER_BLOCK>>>(d_network, d_groupScheduler);

		// reset active signals even for groups that were not scheduled
		// this call could be made unnecessary by smarter scheduling (keep groups scheduled for one more iteration)
		// (and certainly is not necessary when all groups are scheduled for processing)
		const int resetBlocks = (NEURON_COUNT + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;
		resetActiveSignals<<<resetBlocks, THREADS_PER_BLOCK>>>(d_network);

		// activate neurons with AC over treshold and mark active groups for the next iteration
		propagateSignals<<<scheduledBlocks, THREADS_PER_BLOCK>>>(d_network, d_groupScheduler);

		cerr = cudaMemcpy(h_groupScheduler, d_groupScheduler, sizeof(GroupScheduler), cudaMemcpyDeviceToHost);
		RETURN_IF_CUDA_ERR(cerr);

		if (wasOutputGroupScheduled)
		{
			h_network->copyOutputSignalsFromCudaDevice(d_network);

			if (VERBOSE)
				h_network->printActiveOutputNeurons();
		}
	}

	cudaFree(d_groupScheduler);
	cudaFree(d_network);

	cudaDeviceReset();

	delete h_groupScheduler;
	delete h_network;

	return OK;
}

#define RUN_TEST(testFunc)  \
		{ \
			printf("running test: %s\n", #testFunc); \
			err_t testErr = testFunc(); \
			if (testErr) return testErr; \
		}

err_t runTests()
{
	RUN_TEST(testParamAssertions);
	RUN_TEST(testRandomFunctions);
	RUN_TEST(testAddNeuronSynapses);
	RUN_TEST(testInitInputGroup);

	return OK;
}

int main(int argc, char **argv)
{
	srand((unsigned)time(NULL));

	err_t err = runTests();
	if (err) return EXIT_FAILURE;
	printf("Tests OK\n");

	return simulationMain();
}
